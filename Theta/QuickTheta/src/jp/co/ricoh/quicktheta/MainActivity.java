package jp.co.ricoh.quicktheta;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import jp.co.ricoh.quicktheta.view.LogView;

public class MainActivity extends Activity {

	private SharedPreferences  sp; 
	private LogView logViewer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		sp = PreferenceManager.getDefaultSharedPreferences(this);
		
		final Editor sped = sp.edit();
		Button btnStart = (Button) findViewById(R.id.btnStart);
		btnStart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(getApplicationContext(),AutoCruiseActivity.class);
				startActivity(intent);
			}
		});		
		ToggleButton tbWiFi = (ToggleButton) findViewById(R.id.tbWiFiOn);
		tbWiFi.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				sped.putBoolean(SettingActivity.KEY_REVERTWIFI, isChecked);
				sped.commit();
			}
		});

		ToggleButton tbConnect = (ToggleButton) findViewById(R.id.tbTheta);
		tbConnect.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				sped.putBoolean(SettingActivity.KEY_REVERTCONNECTION, isChecked);
				sped.commit();
			}
		});
		ToggleButton tbShoot = (ToggleButton) findViewById(R.id.tbShoot);		
		tbShoot.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				sped.putBoolean(SettingActivity.KEY_SHOOT, isChecked);
				sped.commit();
			}
		});	
		ToggleButton tbAfterShoot = (ToggleButton) findViewById(R.id.tbAfterShoot);		
		tbAfterShoot.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				sped.putBoolean(SettingActivity.KEY_AFTERSHOOT, isChecked);
				sped.commit();
			}
		});	
	}
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			startActivity(new Intent(this, SettingActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		RevertUi();
	}
	
	private void appendLogView(final String log) {
		runOnUiThread(new Runnable() {
			public void run() {
				logViewer.append(log);
			}
		});
	}
	
	private void RevertUi() {
		ToggleButton tbWifi = (ToggleButton) findViewById(R.id.tbWiFiOn);		
		tbWifi.setChecked(sp.getBoolean(SettingActivity.KEY_REVERTWIFI, false));

		ToggleButton tbConnect = (ToggleButton) findViewById(R.id.tbTheta);		
		tbConnect.setChecked(sp.getBoolean(SettingActivity.KEY_REVERTCONNECTION, false));
		
		ToggleButton tbShoot = (ToggleButton) findViewById(R.id.tbShoot);		
		tbShoot.setChecked(sp.getBoolean(SettingActivity.KEY_SHOOT, false));

		ToggleButton tbAfterShoot = (ToggleButton) findViewById(R.id.tbAfterShoot);		
		tbAfterShoot.setChecked(sp.getBoolean(SettingActivity.KEY_AFTERSHOOT, false));
	}
	
}