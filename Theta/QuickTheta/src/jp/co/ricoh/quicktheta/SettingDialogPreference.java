package jp.co.ricoh.quicktheta;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

public class SettingDialogPreference extends DialogPreference {

	EditText edit;
	public SettingDialogPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SettingDialogPreference(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected View onCreateDialogView() {
		this.edit = new EditText(this.getContext());
		this.edit.setText(getPersistedString("default"));
		return this.edit;
	}

	@Override
	protected void onDialogClosed(boolean positiveResult) {
		if(positiveResult){
			persistString(this.edit.getText().toString());
		}
		super.onDialogClosed(positiveResult);
	}
}
