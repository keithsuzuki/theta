package jp.co.ricoh.quicktheta;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class SettingActivity extends PreferenceActivity {
	public static final String KEY_SHUTTERSOUND = "key_ShutterSound";
	public static final String KEY_PWDPREFIX = "key_PwdPrefix";
	public static final String KEY_MYTHETA = "key_MyTheta";
	public static final String KEY_MYPWD = "key_MyPwd";
	public static final String KEY_REVERTWIFI = "key_RevertWifi";
	public static final String KEY_REVERTCONNECTION = "key_RevertConnection";
	public static final String KEY_SHOOT = "key_Shoot";
	public static final String KEY_AFTERSHOOT = "key_AfterShoot";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
	}
}
