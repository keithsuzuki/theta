package jp.co.ricoh.quicktheta;

import java.io.IOException;
import java.util.List;

import com.theta360.lib.PtpipInitiator;
import com.theta360.lib.ThetaException;
import com.theta360.lib.ptpip.entity.Response;
import com.theta360.lib.ptpip.eventlistener.PtpipEventListener;

import jp.co.ricoh.quicktheta.view.LogView;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;

public class AutoCruiseActivity extends Activity {
	private final String strTheta = "THETA";
	private final Handler handler = new Handler();

	private LogView logViewer;
	private SharedPreferences  sp; 
	private WifiManager wifiMan;
	private ConnectivityManager conMan;
	
	private Wifi_Status wifiStatus = Wifi_Status.UNKNOWN;
	private Connect_Status connectStatus = Connect_Status.UNKNOWN;
	private Shoot_Status shootStatus = Shoot_Status.UNKNOWN;
	private int nwIdOld = -2;
	private String strSSID;
	private String strMyTheta;
	boolean shutterSound = false;
	boolean revertWifi = false;
	boolean revertConnection = false;
	boolean shoot = false;
	boolean paused = false;
	boolean goAnotherApp = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auto_cruise);
		
		// initialize utilities
		sp = PreferenceManager.getDefaultSharedPreferences(this);
		wifiMan = (WifiManager)getSystemService(WIFI_SERVICE);
		conMan = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

		// initialize UI component
		logViewer = (LogView) findViewById(R.id.log_view_autocruise);
		Button btnCancel = (Button) findViewById(R.id.btnAutoCruiseCancel);
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				logViewer.append("Cancel button pressed.");
				finish();
			}
		});
		ToggleButton tbPause = (ToggleButton) findViewById(R.id.tbAutoCruisePause);
		tbPause.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				logViewer.append("Pause button pressed.");
				if(paused) {
					// restart auto cruise
					logViewer.append("resume from start.");
					handler.post( AutoCruiseTask );
				}
				paused = !paused;
			}
		});
		
		// load settings from preferences
		shutterSound = sp.getBoolean( SettingActivity.KEY_SHUTTERSOUND, false );
		strMyTheta = sp.getString(SettingActivity.KEY_MYTHETA, "");
		revertWifi = sp.getBoolean(SettingActivity.KEY_REVERTWIFI, false) &&
			!wifiMan.isWifiEnabled();
		revertConnection = sp.getBoolean(SettingActivity.KEY_REVERTWIFI, false);
		shoot = sp.getBoolean(SettingActivity.KEY_SHOOT, false);
		goAnotherApp = sp.getBoolean(SettingActivity.KEY_AFTERSHOOT, false);
		if(goAnotherApp) {
			revertWifi = false;
			revertConnection = false;
		}
		
		// set auto cruise initial status
		wifiStatus = Wifi_Status.TURNON;
		connectStatus = Connect_Status.CONNECT;
		if(shoot) {
			shootStatus = Shoot_Status.SHOOT;			
		} else {
			shootStatus = Shoot_Status.IDLE;
		}
		
		// start auto cruise
		handler.post( AutoCruiseTask );
	}
	
	private void appendLogView(final String log) {
		runOnUiThread(new Runnable() {
			public void run() {
				logViewer.append(log);
			}
		});
	}
	
	private static enum Wifi_Status {
		OFF, TURNON, WAITINGON, ON, TURNOFF, WAITINGOFF, UNKNOWN
	}
	private static enum Connect_Status {
		DISCONNECTED, CONNECT, WAITINGCONNECT, CONNECTED, DISCONNECT, WAITINGDISCONNECT, UNKNOWN
	}
	private static enum Shoot_Status {
		IDLE, SHOOT, WAITINGSHOOT, SHOOTED, UNKNOWN
	}
	
	private final Runnable AutoCruiseTask = new Runnable() {
		@Override
		public void run() {
			boolean loop = true;
			boolean exit = false;

			// check pause
			if(paused) {
				logViewer.append("ACT: paused, reset status.");
				// reset auto cruise initial status
				wifiStatus = Wifi_Status.TURNON;
				connectStatus = Connect_Status.CONNECT;
				shootStatus = Shoot_Status.SHOOT;
				return;
			}
			
			// turn on WiFi
			switch(wifiStatus) {				
			case TURNON:
				logViewer.append("ACT: wifiStatus TURNON.");
				wifiStatus = Wifi_Status.WAITINGON;
				handler.post( WiFiOnTask );
				exit = true;
				break;

			case TURNOFF:
				logViewer.append("ACT: wifiStatus TURNOFF.");
				wifiStatus = Wifi_Status.WAITINGOFF;
				handler.post( WiFiOffTask );
				exit = true;
				break;
				
			case WAITINGON:
				logViewer.append("ACT: wifiStatus WAITINGON.");
				// do nothing and exit to wait
				exit = true;
				break;
				
			case WAITINGOFF:
				logViewer.append("ACT: wifiStatus WAITINGOFF.");
				// do nothing and exit to wait
				exit = true;
				break;
				
			case OFF:
				logViewer.append("ACT: wifiStatus OFF.");
				break;
				
			case ON:
				logViewer.append("ACT: wifiStatus ON.");
				break;

			case UNKNOWN:
				logViewer.append("ACT: wifiStatus UNKNOWN.");
				break;

			default:
				logViewer.append("ERR: ACT: wifiStatus is not defined.");
				exit = true;
				break;
			}
			if(exit) {
				if(loop) handler.postDelayed( AutoCruiseTask, 1000 );
				return;
			}
			
			// connect to theta
			switch(connectStatus) {
			case CONNECT:
				logViewer.append("ACT: connectStatus CONNECT.");
				connectStatus = Connect_Status.WAITINGCONNECT;
				handler.post( ConnectTask );
				exit = true;
				break;
				
			case DISCONNECT:
				logViewer.append("ACT: connectStatus DISCONNECT.");
				connectStatus = Connect_Status.WAITINGDISCONNECT;
				handler.post( DisconnectTask );
				exit = true;
				break;
				
			case WAITINGCONNECT:
				// do nothing and exit to wait
				logViewer.append("ACT: connectStatus WAITINGCONNECT.");
				exit = true;
				break;
				
			case WAITINGDISCONNECT:
				// do nothing and exit to wait
				logViewer.append("ACT: connectStatus WAITINGDISCONNECT.");
				exit = true;
				break;
				
			case CONNECTED:
				logViewer.append("ACT: connectStatus CONNECTED.");
				break;

			case DISCONNECTED:
				logViewer.append("ACT: connectStatus DISCONNECTED.");
				break;

			case UNKNOWN:
				logViewer.append("ACT: connectStatus UNKNOWN.");
				break;

			default:
				logViewer.append("ERR: ACT: connectStatus is not defined.");
				exit = true;
				break;
			}
			if(exit) {
				if(loop) handler.postDelayed( AutoCruiseTask, 1000 );
				return;
			}
		
			// shoot
			switch(shootStatus) {
			case SHOOT:
				logViewer.append("ACT: shootStatus SHOOT.");
				shootStatus = Shoot_Status.WAITINGSHOOT;
				new ShootTask().execute();
				exit = true;
				break;
				
			case WAITINGSHOOT:
				// do nothing and exit to wait
				logViewer.append("ACT: shootStatus WAITINGSHOOT.");
				exit = true;
				break;
				
			case IDLE:
				logViewer.append("ACT: shootStatus IDLE.");
				finish();
				break;

			case SHOOTED:
				logViewer.append("ACT: shootStatus SHOOTED.");
				if(revertConnection && (connectStatus != Connect_Status.DISCONNECTED))
					connectStatus = Connect_Status.DISCONNECT;
				else if(revertWifi && wifiStatus != Wifi_Status.OFF)
					wifiStatus = Wifi_Status.TURNOFF;
				else {
					if(goAnotherApp) {
						Intent intent = new Intent();
						intent.setClassName("com.theta", "com.theta.TitleActivity");
						startActivity(intent);
					}
					loop = false;
				}
				break;

			case UNKNOWN:
				logViewer.append("ACT: shootStatus UNKNOWN.");
				break;

			default:
				logViewer.append("ERR: ACT: shootStatus is not defined.");
				exit = true;
				break;				
			}
			if(exit) {
				if(loop) handler.postDelayed( AutoCruiseTask, 1000 );
				return;
			}
			
			// if shooted, check if this loop should end
			if(shootStatus == Shoot_Status.SHOOTED) {
				// if revert wifi/connection is set, wait for it				
				if((!revertConnection ||
					(revertConnection && (connectStatus == Connect_Status.DISCONNECTED))) &&
					(!revertWifi ||
					(revertWifi && (wifiStatus == Wifi_Status.OFF)))) {
					loop = false;
				}
			}
	
			// continue 
			if(loop) {
				handler.postDelayed( AutoCruiseTask, 1000 );
			} else {
				logViewer.append("ACT: finish task.");
				finish();
			}
		}
    };
    
    private final Runnable WiFiOnTask = new Runnable() {
        @Override
        public void run() {
    		// turning WiFi on
			logViewer.append("WiFiOnTask: going to turn on wifi.");
			wifiMan.setWifiEnabled(true);
			// wait until  wifi is on
			try {
				while ( !wifiMan.isWifiEnabled() ) {
					logViewer.append("WiFiOnTask: waiting for wifi on...");
					Thread.sleep(1000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
    		wifiStatus = Wifi_Status.ON;
        }
    };

    private final Runnable WiFiOffTask = new Runnable() {
        @Override
        public void run() {
    		// turning WiFi off
			logViewer.append("WiFiOffTask: going to turn off wifi.");
			wifiMan.setWifiEnabled(false);
			// wait until  wifi is of
			try {
				while ( wifiMan.isWifiEnabled() ) {
					logViewer.append("WiFiOffTask: waiting for wifi off...");
					Thread.sleep(1000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
    		wifiStatus = Wifi_Status.OFF;
        }
    };

    private final Runnable ConnectTask = new Runnable() {
        @Override
        public void run() {
			// check pause
			if(paused) {
				logViewer.append("ConnectTask: paused");
				return;
			}
			
			// save current wifi id
        	int nwIdTheta = -1;
        	if((wifiStatus == Wifi_Status.ON) && (nwIdOld == -2)) {
        		nwIdOld = wifiMan.getConnectionInfo().getNetworkId();
        		logViewer.append("ConnectTask: current network ID is " + nwIdOld);
        	}
        	
			// search if theta SSID is broadcasted
			wifiMan.startScan();
    		List<ScanResult> results = wifiMan.getScanResults();
    		if(results == null) {
    			logViewer.append( "ConnectTask: Scan result is not ready yet." );
    			handler.postDelayed( ConnectTask, 1000 );
    			return;
    		}
        	final String[] items = new String[results.size()];
        	for( int i = 0; i < results.size(); ++i ) {
        		items[i] = results.get(i).SSID;
        		logViewer.append("ConnectTask: scanned SSID " + items[i]);
        		if( items[i].startsWith( strTheta ) ) {
        			strSSID = items[i];
        			logViewer.append( "ConnectTask: find THETA at " + strSSID );
        			// if my theta is found, stop searching.
        			if( strSSID.equalsIgnoreCase(strMyTheta) ) {
            			logViewer.append( "ConnectTask: found MYTHETA at " + strSSID );
        				break;
        			}
        		}
        	}
        	
        	// if no theta found, try again
        	if((strSSID == null) || !strSSID.startsWith( strTheta )) {
        		logViewer.append( "ConnectTask: no theta SSID is broadcasted." );
    			handler.postDelayed( ConnectTask, 1000 );
        		return;
        	}
        	// if MyTheta is set and it's not found, try again
        	if((strMyTheta.length() > 0) && !strMyTheta.equalsIgnoreCase(strSSID)) {
        		logViewer.append( "ConnectTask: MyTheta is set, but it's not found." );
    			handler.postDelayed( ConnectTask, 1000 );
        		return;        		
        	}
        	
        	// create config for the theta
        	WifiConfiguration wcNew = new WifiConfiguration();
    		//SSID
        	wcNew.SSID = "\"" + strSSID + "\"";
    		//このコンフィギュレーションで管理する認証キー
        	wcNew.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
    		//IEEE 802.11認証アルゴリズム
//        	wcNew.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
    		//セキュリティプロトコル
        	wcNew.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        	wcNew.allowedProtocols.set(WifiConfiguration.Protocol.RSN);//WPA2
    		//認証されたグループの暗号
        	wcNew.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        	wcNew.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        	wcNew.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        	wcNew.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
    		//WPA認証用ペア暗号
        	wcNew.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        	wcNew.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        	//WPA用共通キー
    		String strMyPassword = sp.getString(SettingActivity.KEY_MYPWD, "");    			
    		if( strMyTheta.equals(strSSID) && (strMyPassword.length() > 0)) {
    			wcNew.preSharedKey = strMyPassword;
    		} else {
    			String strPwdPrefix = sp.getString(SettingActivity.KEY_PWDPREFIX, "00");
    			wcNew.preSharedKey = "\"" + strPwdPrefix + strSSID.substring(strSSID.length() - "123456".length()) + "\"";
    		}
    		wcNew.hiddenSSID = true;
    		wcNew.status = WifiConfiguration.Status.ENABLED;
			logViewer.append( "ConnectTask: preSharedKey is " + wcNew.preSharedKey );
    		//設定済のネットワークに新しいネットワーク情報を追加する
    		nwIdTheta = wifiMan.addNetwork(wcNew);
    		if( nwIdTheta == -1 ){ 
    			// 失敗した場合-1となる
    			logViewer.append( "ERR: ConnectTask: failed to add config " + strSSID );
    			handler.postDelayed( ConnectTask, 1000 );
    			return;
    		};
			logViewer.append( "ConnectTask: theta config is saved with ID " + nwIdTheta );
			wcNew.networkId = nwIdTheta;
    		//設定されたネットワーク情報をこの端末に保存する
    		wifiMan.saveConfiguration();
    		wifiMan.updateNetwork(wcNew);
    		wifiMan.enableNetwork(wcNew.networkId, true);
    		
    		/*
    		try {
	    		WifiConfiguration wc = new WifiConfiguration();
	    		wc = wifiMan.getConfiguredNetworks().get(nwIdTheta);
	    		logViewer.append( "ConnectTask:...switching to theta " + wcNew.SSID );
	    		wifiMan.updateNetwork(wc);
	            wifiMan.enableNetwork(wcNew.networkId, true);
    		} catch (NullPointerException e) {
    			e.printStackTrace();
    		}
    		*/
    		
    		// wait until connect to theta
			handler.post( VerifyConnectionTask );
        }
    };

    private final Runnable VerifyConnectionTask = new Runnable() {
        @Override
        public void run() {
			// check pause
			if(paused) {
				logViewer.append("VerifyTask:...paused");
				return;
			}
			
			//
			wifiMan.startScan();

			//
			if (wifiMan.getConnectionInfo().getSupplicantState() == SupplicantState.COMPLETED) {
				NetworkInfo ni = conMan.getActiveNetworkInfo();
				if(ni == null) {
					logViewer.append("VerifyConnectionTask: Wifi connect try : NetworkInfo is null");
					handler.postDelayed( VerifyConnectionTask, 1000 );
					return;
				}
				if(ni.getType() != ConnectivityManager.TYPE_WIFI) {
					logViewer.append("VerifyConnectionTask: Wifi connect try : NetworkInfo is not wifi");
					handler.postDelayed( VerifyConnectionTask, 1000 );
					return;
				}
				if(ni.getState() != State.CONNECTED) {
					logViewer.append("VerifyConnectionTask: Wifi connect try : Wifi state is not connected");
					handler.postDelayed( VerifyConnectionTask, 1000 );
					return;
				}
				if( (strSSID == null) ||
					(!wifiMan.getConnectionInfo().getSSID().equalsIgnoreCase(strSSID)) &&
					(!wifiMan.getConnectionInfo().getSSID().equalsIgnoreCase("\"" + strSSID + "\""))) {
					logViewer.append("VerifyConnectionTask: Wifi connect try : connected another SSID " + wifiMan.getConnectionInfo().getSSID() );
					wifiMan.reconnect();
					handler.postDelayed( VerifyConnectionTask, 1000 );
					return;
				}
				connectStatus = Connect_Status.CONNECTED;						
			}
			else
				handler.postDelayed( VerifyConnectionTask, 1000 );
		}
	};

    private final Runnable DisconnectTask = new Runnable() {
        @Override
        public void run() {
			if(nwIdOld >= 0) {
				wifiMan.enableNetwork(nwIdOld, true);
				for (WifiConfiguration wc : wifiMan.getConfiguredNetworks()) {
					wifiMan.enableNetwork(wc.networkId, false);
				}
			}
			connectStatus = Connect_Status.DISCONNECTED;						
        }
    };
    
	private static enum ShootResult {
		SUCCESS, FAIL_CAMERA_DISCONNECTED, FAIL_STORE_FULL, FAIL_DEVICE_BUSY
	}
	
	private class ShootTask extends AsyncTask<Void, Void, ShootResult> {
		PtpipInitiator camera;
		
		@Override
		protected ShootResult doInBackground(Void... params) {
			CaptureListener postviewListener = new CaptureListener();

			try {
				camera = new PtpipInitiator("192.168.1.1");
				camera.setAudioVolume( shutterSound ? 10 : 0 );
				camera.initiateCapture(postviewListener);
				
				return ShootResult.SUCCESS;
			} catch (IOException e) {
				return ShootResult.FAIL_CAMERA_DISCONNECTED;
			} catch (ThetaException e) {
				if (Response.RESPONSE_CODE_STORE_FULL == e.getStatus()) {
					return ShootResult.FAIL_STORE_FULL;
				} else if (Response.RESPONSE_CODE_DEVICE_BUSY == e.getStatus()) {
					return ShootResult.FAIL_DEVICE_BUSY;
				} else {
					return ShootResult.FAIL_CAMERA_DISCONNECTED;
				}
			}
		}

		@Override
		protected void onPostExecute(ShootResult result) {
			if (result == ShootResult.FAIL_CAMERA_DISCONNECTED) {
				logViewer.append("initiateCapture:FAIL_CAMERA_DISCONNECTED");
			} else if (result == ShootResult.FAIL_STORE_FULL) {
				logViewer.append("initiateCapture:FAIL_STORE_FULL");
			} else if (result == ShootResult.FAIL_DEVICE_BUSY) {
				logViewer.append("initiateCapture:FAIL_DEVICE_BUSY");
			} else if (result == ShootResult.SUCCESS) {
				logViewer.append("initiateCapture:SUCCESS");
			}
		}
		
		private class CaptureListener extends PtpipEventListener {
			private int latestCapturedObjectHandle;
			private boolean objectAdd = false;

			@Override
			public void onObjectAdded(int objectHandle) {
				this.objectAdd = true;
				this.latestCapturedObjectHandle = objectHandle;
				appendLogView("ShootTask:...objectAdded, ObjectHandle = " + latestCapturedObjectHandle);
				new CloseCameraTask().execute();
			}
			
			@Override
			public void onCaptureComplete(int transactionId) {
				appendLogView("ShootTask:...shoot complete. " + latestCapturedObjectHandle);
			}
		}		
	}
	
	private class CloseCameraTask extends AsyncTask<Void, String, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {

			try {
				PtpipInitiator.close();
				publishProgress("CloseCameraTask:...camera closed.");
				shootStatus = Shoot_Status.SHOOTED;
				return true;

			} catch (Throwable throwable) {
				String errorLog = Log.getStackTraceString(throwable);
				publishProgress(errorLog);
				return false;
			}
		}

		@Override
		protected void onProgressUpdate(String... values) {
			for (String log : values) {
				logViewer.append(log);
			}
		}
	}
	
	
	private class DisConnectTask extends AsyncTask<Void, String, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				PtpipInitiator.close();
				
				// set back wifi configurations.
				if(nwIdOld >= 0) {
					wifiMan.enableNetwork(nwIdOld, true);
					for (WifiConfiguration wc : wifiMan.getConfiguredNetworks()) {
						wifiMan.enableNetwork(wc.networkId, false);
					}
				}			
				return true;
			} catch (Throwable throwable) {
				String errorLog = Log.getStackTraceString(throwable);
				publishProgress(errorLog);
				return false;
			}
		}
	}
}